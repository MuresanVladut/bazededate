﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private SqlConnection connection;
        private SqlDataAdapter adapter;
        private DataSet dataSet;
        
        public Form1()
        {
            InitializeComponent();
            Form1_Load();
            display();
        }

        private void Form1_Load()
        {
            connection= new SqlConnection("Data Source=DESKTOP-PBM8V3I\\SQLEXPRESS; Initial Catalog=Hearthstone; Integrated Security=True");
            adapter=new SqlDataAdapter();
            dataSet=new DataSet();
        }

        private void display()
        {
            adapter.SelectCommand=new SqlCommand("Select * from factions",connection);
            dataSet.Clear();
            adapter.Fill(dataSet);
            dataGridView1.DataSource = dataSet.Tables[0];

            dataSet=new DataSet();
            adapter.SelectCommand = new SqlCommand("select cards.idNew, cards.name,cards.textt,factions.factionName from cards inner join factions on factionID=factions.id", connection);
            dataSet.Clear();
            adapter.Fill(dataSet);
            dataGridView2.DataSource = dataSet.Tables[0];

        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex];
                

                adapter.InsertCommand=new SqlCommand("Insert into cards values (@name,@text,null,null,null,@factionID,null,null,null,null,null,null)",connection);
                adapter.InsertCommand.Parameters.Add("@name", SqlDbType.VarChar).Value = textBox1.Text;
                adapter.InsertCommand.Parameters.Add("@text", SqlDbType.VarChar).Value = textBox2.Text;
                adapter.InsertCommand.Parameters.Add("@factionID", SqlDbType.Int).Value = row.Cells["id"].Value;
                connection.Open();
                adapter.InsertCommand.ExecuteNonQuery();
                display();
                connection.Close();


            }
        }

    

     



        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];


                dataSet = new DataSet();
                adapter.SelectCommand =
                    new SqlCommand(
                        "select cards.idNew, cards.name,cards.textt,factions.factionName from cards inner join factions on factionID=factions.id where factionID=@parametru",
                        connection);
                adapter.SelectCommand.Parameters.Add("@parametru", SqlDbType.Int).Value = row.Cells["id"].Value.ToString();
                dataSet.Clear();
                adapter.Fill(dataSet);
                dataGridView2.DataSource = dataSet.Tables[0];
            
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dataGridView2.Rows[dataGridView2.CurrentCell.RowIndex];


                adapter.DeleteCommand = new SqlCommand("delete from cards where cards.idNew=@idd", connection);
                adapter.DeleteCommand.Parameters.Add("@idd", SqlDbType.Int).Value = row.Cells["idNew"].Value;
                connection.Open();
                adapter.DeleteCommand.ExecuteNonQuery();
                display();
                connection.Close();


            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dataGridView2.Rows[dataGridView2.CurrentCell.RowIndex];


                adapter.UpdateCommand = new SqlCommand("update cards set cards.name=@cardsName,cards.textt=@cardsText,cards.factionID=@factionID where cards.idNew=@cardsID", connection);
                adapter.UpdateCommand.Parameters.Add("@cardsName", SqlDbType.VarChar).Value = textBox1.Text;
                adapter.UpdateCommand.Parameters.Add("@cardsText", SqlDbType.VarChar).Value = textBox2.Text;
                adapter.UpdateCommand.Parameters.Add("@factionID", SqlDbType.VarChar).Value = textBox3.Text;
                adapter.UpdateCommand.Parameters.Add("@cardsID", SqlDbType.VarChar).Value = row.Cells["idNew"].Value;
                connection.Open();
                adapter.UpdateCommand.ExecuteNonQuery();
                display();
                connection.Close();


            }

        }

       
    }
    }
